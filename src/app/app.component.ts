import { Component } from '@angular/core';
import $ from "jquery";
import { faHome, faUser, faList, faBook, faUsers, faComments, faCog, faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  faHome = faHome;
  cracha = faUser;
  lista = faList;
  livros = faBook;
  equipe = faUsers;
  comunicacao = faComments;
  config = faCog;
  barras = faBars;


  colapse() {
    $('#sidebar').toggleClass('active');
  }
}
