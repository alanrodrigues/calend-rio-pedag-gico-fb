import { Component, OnInit, ViewChild } from '@angular/core';
import { IgxCalendarComponent, DateRangeType } from 'igniteui-angular';
import { CalendarioService } from '../services';

@Component({
    selector: 'app-visualizar',
    templateUrl: './visualizar.component.html',
    styleUrls: ['./visualizar.component.scss']
})
export class VisualizarComponent implements OnInit {

    dates = [
        new Date(2019, 11,  10),
        new Date(2019, 11, 11),
        new Date(2019, 11, 30),
        new Date(2019, 11, 31)
    
];
    public locale: "fr";
    public eventos;

    constructor(private servico: CalendarioService) {

    }

    async ngOnInit() {
        this.eventos = await this.servico.getEventos();
        console.log(this.eventos)

    }

    public async selecionarEvento(valor){
        let a = await this.servico.getAtividadesPorEvento(valor);
    }



}
