import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalendarioService {

  private readonly BASE_URL = "/api";

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  }

  async getAtividadesResponsavel(ano?: number, mes?: number) {

    let data = "";

    if (ano != undefined && mes == undefined) {
      data = "?ano=" + +ano;
    } else if (ano == undefined && mes != undefined) {
      data = "?mes=" + +mes;
    } else if (ano != undefined && mes != undefined) {
      data = "?ano=" + +ano + "&mes=" + +mes;
    } else {
      data = "";
    }


    return await this.http.get<any>(this.BASE_URL + "/responsavel/2/atividades" + data, this.httpOptions).toPromise();

  }

  async getResponsavelStatus(ano?: number, mes?: number) {
    let data = "";

    if (ano != undefined && mes == undefined) {
      data = "?ano=" + +ano;
    } else if (ano == undefined && mes != undefined) {
      data = "?mes=" + +mes;
    } else if (ano != undefined && mes != undefined) {
      data = "?ano=" + +ano + "&mes=" + +mes;
    } else {
      data = "";
    }
    return await this.http.get<any>(this.BASE_URL + "/responsavel/2/atividades/status" + data, this.httpOptions).toPromise();
  }

  async getMarcarConcluir(idTarefa: number, responsavelAtividade: number): Promise<any> {
    return await this.http.put<any>(this.BASE_URL + "/responsavel/" + responsavelAtividade + "/atividades/" + idTarefa + "/concluir", this.httpOptions).toPromise();
  }

  async enviarArquivo(arquivo: File) {

    const formData: FormData = new FormData();
    formData.append('arquivo', arquivo);
    return await this.http.post(this.BASE_URL + "/responsavel/" + 2 + "/atividades/" + 2 + "/enviar", formData).subscribe();

  }

  async getAtividadesPorEvento(idEvento?: number) {
    return await this.http.get(this.BASE_URL + "/responsavel/2/eventos/" + idEvento + "/atividades").toPromise();
  }

  async getEventos() {
    return await this.http.get(this.BASE_URL + "/eventos").toPromise();
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
