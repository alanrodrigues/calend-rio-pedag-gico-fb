import { Tarefa } from './tarefa.model';

export class Atividades {
    constructor(public status?: string, public tarefa?: Tarefa[],public concluida?: boolean, public atrasada?: boolean, 
        public DataInicio?: string, public DataFim?: string, public ConcluidoEm?: string, public ResponsavelId?: number, public Id?: number) { }
}