import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio';
import { CalendarioService } from './services';
import { AtividadesComponent } from './atividades';
import { RelatorioComponent } from './relatorio';
import { MinhaEquipeComponent } from './minha-equipe';
import { ComunicacaoComponent } from './comunicacao';
import { ConfiguracaoComponent } from './configuracao';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { ProgressbarModule } from 'ngx-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FileUploadModule } from 'primeng/fileupload';
import { IgxCalendarModule, IgxDialogModule, IgxCardModule, IgxIconModule } from 'igniteui-angular';
import { VisualizarComponent } from './visualizar';


@NgModule({
    declarations: [
        InicioComponent,
        AtividadesComponent,
        RelatorioComponent,
        MinhaEquipeComponent,
        ComunicacaoComponent,
        ConfiguracaoComponent,
        VisualizarComponent
    ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        CalendarModule,
        FormsModule,
        TriStateCheckboxModule,
        ProgressbarModule.forRoot(),
        FontAwesomeModule,
        FileUploadModule,
        IgxCalendarModule,
        IgxDialogModule,
        IgxCardModule,
        IgxIconModule
    ],
    providers: [
        CalendarioService
    ],
})
export class CalendarioModule {
}
