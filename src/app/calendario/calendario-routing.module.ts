import { Routes } from '@angular/router';
import { InicioComponent } from '.';
import { VisualizarComponent } from './visualizar';
import { AtividadesComponent } from './atividades';
import { RelatorioComponent } from './relatorio';
import { MinhaEquipeComponent } from './minha-equipe';
import { ComunicacaoComponent } from './comunicacao';
import { ConfiguracaoComponent } from './configuracao';


export const CalendarioRoutes: Routes = [ 
    {
        path: 'calendario',
        redirectTo: 'calendario/inicio'
    },
    {
        path: 'calendario/inicio',
        component: InicioComponent
    },
    {
        path: 'calendario/visualizar',
        component: VisualizarComponent
    },
    {
        path: 'calendario/atividades',
        component: AtividadesComponent
    },
    {
        path: 'calendario/relatorio',
        component: RelatorioComponent
    },
    {
        path: 'calendario/minha-equipe',
        component: MinhaEquipeComponent
    },
    {
        path: 'calendario/comunicacao',
        component: ComunicacaoComponent
    },
    {
        path: 'calendario/configuracao',
        component: ConfiguracaoComponent
    }

]