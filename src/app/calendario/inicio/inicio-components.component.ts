import { Component, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import Chart from 'chart.js';
import { faFileUpload, faCog, faPaperclip, faHome } from '@fortawesome/free-solid-svg-icons';
import { CalendarioService } from '../services';
import { Atividades, Tarefa } from '../shared';
import { NgForm } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';



@Component({
  selector: 'app-components',
  templateUrl: './inicio-components.component.html',
  styleUrls: ['./inicio-components.component.css']
})
export class InicioComponent {
  @ViewChild('atrasadasCanvas', { static: true }) atrasadasCanvas;
  @ViewChild('feitasCanvas', { static: true }) feitasCanvas;
  @ViewChild('pendentesCanvas', { static: true }) pendentesCanvas;
  @ViewChild('ano', { static: true }) ano: NgForm;
  @ViewChild('closeModal', { static: true }) closeModal: ElementRef;

  public enviarAquivoIcon = faFileUpload;
  public config = faCog;
  public anexo = faPaperclip;
  public data: Date;

  public donuts: any;
  public servicoAtividades: any;
  public statusAtividades: any;
  public fileToUpload: File = null;
  public modalRef: BsModalRef;

  public tarefas: Tarefa[];
  public searchVal: string = '';

  public meses = [
    { nome: "Janeiro", valor: 1 },
    { nome: "Fevereiro", valor: 2 },
    { nome: "Março", valor: 3 },
    { nome: "Abril", valor: 4 },
    { nome: "Maio", valor: 5 },
    { nome: "Junho", valor: 6 },
    { nome: "Julho", valor: 7 },
    { nome: "Agosto", valor: 8 },
    { nome: "Setembro", valor: 9 },
    { nome: "Outubro", valor: 10 },
    { nome: "Novembro", valor: 11 },
    { nome: "Dezembro", valor: 12 }
  ];


  constructor(private servico: CalendarioService, private modalService: BsModalService) {

  }

  public async ngOnInit(): Promise<void> {
    this.meses;
    await this.servico.getAtividadesResponsavel().then((data) => {
      this.servicoAtividades = data;
    });
    await this.servico.getResponsavelStatus().then((data) => {
      this.statusAtividades = data;
      this.donuts = this.graphAtividadesAtrasadas(this.statusAtividades.atrasadas, this.statusAtividades.total);
      this.donuts = this.graphAtividadesFeitas(this.statusAtividades.concluidas, this.statusAtividades.total);
      this.donuts = this.graphAtividadesPendentes(this.statusAtividades.pendentes, this.statusAtividades.total);
    });

  }

  public formataData(date) {
    let data = date.split(" ");
    let dataDividida = data[0].split("-");
    let dia = dataDividida[2];
    let mes;
    this.meses.forEach(dados => {
      if (dados.valor == dataDividida[1]) {
        mes = dados.nome
      }
    });

    return dia + " de " + mes;
  }

  public async enviarArquivo(event) {
    if (event.target.files && event.target.files[0]) {
      const arquivo = event.target.files[0];

      await this.servico.enviarArquivo(arquivo);
    }

  }

  public async checkSearchVal() {
    let teste;
    let tarefa: Tarefa[] = [];
    await this.servicoAtividades.forEach(element => {
      console.log("T " + teste, "E " + element);
      teste = element;
    });
    if (this.searchVal && this.searchVal != '') {

      for (let selectedUser of teste.atividades) {
        console.log("S " + selectedUser)
        if (selectedUser.tarefa.toLowerCase().search(this.searchVal.toLowerCase()) != -1) {
          tarefa.push(selectedUser);
        }
      }

      console.log(tarefa);

      this.tarefas = tarefa.slice();

    }

  }


  public async selecionaAno() {

    await this.servico.getAtividadesResponsavel(+this.ano).then((data) => {
      this.servicoAtividades = data;

    });
    await this.servico.getResponsavelStatus(+this.ano).then((data) => {
      this.statusAtividades = data;
      this.donuts = this.graphAtividadesAtrasadas(this.statusAtividades.atrasadas, this.statusAtividades.total);
      this.donuts = this.graphAtividadesFeitas(this.statusAtividades.concluidas, this.statusAtividades.total);
      this.donuts = this.graphAtividadesPendentes(this.statusAtividades.pendentes, this.statusAtividades.total);
    });

  }

  public async selecionaMes(mes) {
    let ano = undefined
    if (+this.ano == NaN) {
      ano = undefined
    }
    await this.servico.getAtividadesResponsavel(ano, +mes).then((data) => {
      this.servicoAtividades = data;

    });
    await this.servico.getResponsavelStatus(ano, +mes).then((data) => {
      this.statusAtividades = data;

      this.donuts = this.graphAtividadesAtrasadas(this.statusAtividades.atrasadas, this.statusAtividades.total);
      this.donuts = this.graphAtividadesFeitas(this.statusAtividades.concluidas, this.statusAtividades.total);
      this.donuts = this.graphAtividadesPendentes(this.statusAtividades.pendentes, this.statusAtividades.total);
    });

  }

  public async alterarStatus(atividade: Atividades): Promise<void> {

    if (!atividade.concluida) {
      this.modalRef.hide();
      await this.servico.getMarcarConcluir(atividade.Id, atividade.ResponsavelId).then((data) => {
        this.statusAtividades = data;
      });
      await this.servico.getAtividadesResponsavel().then((data) => {
        this.servicoAtividades = data;
      });
      await this.servico.getResponsavelStatus().then((data) => {
        this.statusAtividades = data;
        this.donuts = this.graphAtividadesAtrasadas(this.statusAtividades.atrasadas, this.statusAtividades.total);
        this.donuts = this.graphAtividadesFeitas(this.statusAtividades.concluidas, this.statusAtividades.total);
        this.donuts = this.graphAtividadesPendentes(this.statusAtividades.pendentes, this.statusAtividades.total);
      });



    }



  }

  public abrirModal(template: TemplateRef<any>) {

    let config = {
      class: 'modal-dialog-centered'
    }
    this.modalRef = this.modalService.show(template, config);
  }

  public fecharModal(atividade: Atividades) {
    if (atividade.Id === +$('input[value]:checked').val()) {
      $('input[value=' + atividade.Id + ']:checked').prop('checked', false)
      this.modalRef.hide();
    }
  }

  public calculaResultado(total, valor) {

    if (+total === +valor) {
      total = 0;
    } else {
      total = +total;
    }

    let resultado = (+valor / +total) * 100;
    resultado > 100 ? resultado = 100 : resultado = resultado;
    if (isNaN(resultado)) {
      resultado = 0
    } else {
      resultado = resultado
    }

    return resultado.toFixed(2) + "%";
  }


  public graphAtividadesAtrasadas(atrasadas, total) {

    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });



    let valorTotal = +total - +atrasadas;
    +valorTotal == 0 ? valorTotal = 1 : valorTotal = +valorTotal;

    var config = {
      type: 'doughnut',
      data: {
        labels: ['Atrasadas', 'Total'],
        datasets: [{
          data: [+atrasadas, valorTotal],
          backgroundColor: ['red'],
          borderWidth: [0, 0, 0, 0]
          // hoverBackgroundColor: [
          //   "#000",
          //   "#FFF"
          // ]
        }]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        elements: {
          center: {
            text: this.calculaResultado(total, atrasadas),
            color: '#FFF',
            fontStyle: 'Arial',
            sidePadding: 10
          }
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        }
      }

    };


    var ctx = this.atrasadasCanvas.nativeElement;
    var ts = new Chart(ctx, config);

    return ts;
  }

  public graphAtividadesFeitas(concluidas, total) {
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });
    let valorTotal = +total - +concluidas;
    +valorTotal == 0 ? valorTotal = 0 : valorTotal = +valorTotal;
    var config = {
      segmentShowStroke: false,
      type: 'doughnut',
      data: {
        labels: ['Concluídas', 'Total'],
        datasets: [{
          data: [+concluidas, +valorTotal],
          backgroundColor: ['green'],
          borderWidth: [0, 0, 0, 0]
          // hoverBackgroundColor: [
          //   "#000",
          //   "#FFF"
          // ]
        }]
      },
      options: {
        segmentShowStroke: false,
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        elements: {
          segmentShowStroke: false,
          center: {
            text: this.calculaResultado(total, concluidas),
            color: '#FFF',
            fontStyle: 'Arial',
            sidePadding: 1

          }
        },
        tooltips: {
          segmentShowStroke: false,
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        }
      }
    };

    var ctx = this.feitasCanvas.nativeElement;
    var ts = new Chart(ctx, config);

    return ts;
  }

  public graphAtividadesPendentes(pendentes, total) {
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          var ctx = chart.chart.ctx;

          //Get options from the center object in options
          var centerConfig = chart.config.options.elements.center;
          var fontStyle = centerConfig.fontStyle || 'Arial';
          var txt = centerConfig.text;
          var color = centerConfig.color || '#000';
          var sidePadding = centerConfig.sidePadding || 20;
          var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          var stringWidth = ctx.measureText(txt).width;
          var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          var widthRatio = elementWidth / stringWidth;
          var newFontSize = Math.floor(30 * widthRatio);
          var elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          var fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });

    let valorTotal = +total - +pendentes;
    +valorTotal == 0 ? valorTotal = 1 : valorTotal = +valorTotal;

    var config = {
      type: 'doughnut',
      data: {
        labels: ['Pendentes', 'Total'],
        datasets: [{
          data: [+pendentes, valorTotal],
          backgroundColor: ['yellow'],
          borderWidth: [0, 0, 0, 0]
          // hoverBackgroundColor: [
          //   "#000",
          //   "#FFF"
          // ]
        }]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        elements: {
          center: {
            text: this.calculaResultado(total, pendentes),
            color: '#FFF',
            fontStyle: 'Arial',
            sidePadding: 10
          }
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        }
      }
    };

    var ctx = this.pendentesCanvas.nativeElement;
    var ts = new Chart(ctx, config);

    return ts;

  }



}