import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CalendarioModule } from './calendario';
import { AppRoutingModule } from './app-routing-module';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule, ProgressbarModule } from 'ngx-bootstrap';
import { FullCalendarModule } from 'ng-fullcalendar';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    CalendarioModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    FullCalendarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
