import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalendarioRoutes } from './calendario/calendario-routing.module';

export const routes: Routes = [
    {
        path: '',
        redirectTo: '/calendario/inicio',
        pathMatch: 'full'
    },
    ...CalendarioRoutes
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }
