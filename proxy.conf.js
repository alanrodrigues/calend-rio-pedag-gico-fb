const PROXY_CONFIG = [
    {
        context: ['/api'],
        target: 'http://10.1.61.75:8000',
        secure: false,
        logLevel: 'debug',
        pathRewrite: {'^/api': ''}
    }
];

module.exports = PROXY_CONFIG;